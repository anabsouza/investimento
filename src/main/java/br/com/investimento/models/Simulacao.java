package br.com.investimento.models;

import org.hibernate.validator.constraints.UniqueElements;
import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome do Interessado deve ser informado")
    @NotBlank(message = "Nome do Interessado é necessário")
    private String nomeinteressado;

    @NotNull(message = "Email do Interessado deve ser informado")
    @NotBlank(message = "Email do Interessado é necessário")
    private String email;


    @Digits(integer = 10, fraction = 2, message = "Valor da aplicação fora do padrão")
    @DecimalMin(value = "50.0", message = "Valor minimo da aplicacao")
    private double valorAplicado;

    private int quantidadeMeses;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    ///**** NOME INTERESSADO
    public String getNomeinteressado() {
        return nomeinteressado;
    }

    public void setNomeinteressado(String nomeinteressado) {
        this.nomeinteressado = nomeinteressado;
    }


    ///********** Valor Aplicado
    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    ///**********Quantidade de Meses
    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setquantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;

    }
}
