package br.com.investimento.respositories;


import br.com.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;


public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {
    }


