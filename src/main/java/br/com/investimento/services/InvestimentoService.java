package br.com.investimento.services;

import br.com.investimento.models.Investimento;
import br.com.investimento.respositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service


public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento registrarInvestimento(Investimento investimento){
        Investimento investCollector = investimentoRepository.save(investimento);
        return investCollector;
    }

    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentoRepository.findAll();
    }

}
