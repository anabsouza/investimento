package br.com.investimento.controllers;

import br.com.investimento.services.InvestimentoService;
import br.com.investimento.models.Investimento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

    @RestController
    @RequestMapping("/investimentos")
    public class InvestimentoController {

        @Autowired
        private InvestimentoService investimentoService;

        @PostMapping
        @ResponseStatus(HttpStatus.CREATED)
        public Investimento registrarInvestimento(@RequestBody @Valid Investimento investimento) {
            return investimentoService.registrarInvestimento(investimento);
        }

        @GetMapping
        public Iterable<Investimento> listarOsInvestimentos() {
            return investimentoService.lerTodosOsInvestimentos();
        }

    }




